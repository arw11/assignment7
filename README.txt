
For question 1:
	 type " g++ q1a.cpp -o q1a" to compile
	 type "./q1a" to run
	 x and y points are output to data.txt
	 the key input lines are changing the array function "double *f(double x,double y[N])"; this determines the system of first order ODEs
	 					choosing the dimensionality of ODEs, "const int N"
	 					in the main function: the array of initial conditions "double y0[N]"
	 							      the initial x value "double x"
	 							      the maximum x value "double range"  
	 adaptive step size was not implemented in the end...
	 
For question 2:
	 
	 Part i):
	 type " g++ q2a.cpp -o q2a" to compile
	 type "./q2a" to run
	 data is output to "erf2.txt"
	 type "tail erf2.txt" to see the value of erf(2)
	 
	 Part ii):
	 type " g++ q2b.cpp -o q2b" to compile
	 type "./q2b" to run
	 data is output to "erf2b.txt"
	 type "tail erf2b.txt" to see the value of erf(2)
	 