//This program implements the RK45 method for finding erf(2)
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

const int N = 1; //Dimension of system of first order ODEs
const double pi = acos(-1.0);

double *f(double x,double y[N])
{
	double yp[N]; //Array corresponding to y' vector
	yp[0] = (2.0/sqrt(pi))*exp(-(x*x)) ; //Differential equation for erf(x)
	return yp;
}	 

double *RK(double x, double y[N], double h)
{
    double ynext[N];//This is where y_n+1 from page 40 in the slides will be stored

    for(int I=0; I<N ; I++)//Loop to calculate y_n+1 for each component of the y vector
      {
	double k1 = h * f(x,y)[I], k1y[N];	
	for(int i=0; i<N ; i++)
	{
		k1y[i]=y[i] + (k1/2.);
	}
	double k2 = h * f(x+(h/2.),k1y)[I], k2y[N];
	for(int j=0; j<N ; j++)
	{
			k2y[j]=y[j] + (k2/2.);
	}
	double k3 = h * f(x+(h/2.),k2y)[I], k3y[N];
	for(int k=0; k<N ; k++)
	{
			k3y[k]=y[k] + k3;
	}
	double k4 = h * f(x+(h),k3y)[I];

	ynext[I] = y[I]+(k1/6.)+(k2/3.)+(k3/3.)+(k4/6.);
      }

	return ynext;
}			
	
int main()
{
	int count; //Counts number of function evaluations
	double y0[N];
	y0[0]=0; //Modify initial conditions here

	double eps = 0.000001;
	double deltax = 0.1;
	double x=0.0; //Modify initial x here
	double range = 2.0; //Modify final x here
	double y1[N],y2[N],ynext[N];//Creates number of steps between x and range given deltax

	ofstream data("erf2.txt");

//Calculates y1 for step doubling
	for(int M=0; M < N; M++)
	{
		y1[M] = RK(x,y0,deltax)[M];
	}

//Write intiial conditions to erf2.txt
        for(int D=0; D < N; D++)
        {
                data << x << "\t" << y0[D] << "\t";
        }
        data << endl;

	count = 1;
	
//This loop performs the step doubling process whilst calling the Runge-Kutta method
	while(x<range)
	{
		for(int B=0; B<N ; B++) //Calculates y2 for step doubling method, this loop goes halfway to x+deltax
		{
			y2[B] = RK(x,y0,deltax/2.0)[B];
		}
	
		for(int A=0; A<N ; A++) //y2 for step doubling method, this loop completes the jump to x+deltax
		{
			y2[A] = RK(x+deltax/2.0,y2,deltax/2.0)[A];
		}
		
		count = count + 2;
	
		while(abs(y2[0]-y1[0]) > eps)
		{
			deltax = deltax*pow((eps/((abs(y2[0]-y1[0])))),0.2);

			for(int V=0; V < N; V++) //calculates y1 array for step doubling
	                {
        	        y1[V] = RK(x,y0,deltax)[V];
                	}
			
			for(int B=0; B<N ; B++)
               	 	{
                        y2[B] = RK(x,y0,deltax/2.0)[B];
               	 	}

                	for(int A=0; A<N ; A++) 
                	{
                        y2[A] = RK(x,y2,deltax/2.0)[A];
               		 }
		
			count = count + 3;
		}
		
		for(int j=0; j<N ; j++) //Calculates y_n+1 from page 54 in the lecture slides
                        {
                        ynext[j] = y2[j] + ((y2[j]-y1[j])/15.);
                        }


		if(x+deltax > range)
                {
                        deltax = range - x;

                       for(int V=0; V < N; V++)
                        {
                        y1[V] = RK(x,y0,deltax)[V];
                        }

                        for(int B=0; B<N ; B++)
                        {
                        y2[B] = RK(x,y0,deltax/2.0)[B];
                        }

                        for(int A=0; A<N ; A++)
                        {
                        y2[A] = RK(x+deltax/2.0,y2,deltax/2.0)[A];
                        }

                        for(int j=0; j<N ; j++) //Calculates y_n+1 from page 54 in the lecture slides
                        {
                        ynext[j] = y2[j] + ((y2[j]-y1[j])/15.);
                        }

                        x=x+deltax; //Sets x to the next point in the iteration 

			for(int E=0; E < N; E++) //Writes the output data to erf2.txt in the following format: x        y[0]    x       y[2]    x       ...
	                {
                        data << x << "\t" << ynext[E] << "\t";
        	        }

                	data << endl;

			count = count + 3;
                }


		if(x+deltax <= range)
		{
		 x=x+deltax; //Sets x to the next point in the iteration 
		
		 for(int E=0; E < N; E++) //Writes the output data to erf2.txt in the following format: x        y[0]    x       y[2]    x       ...
		   {
                        data << x << "\t" << ynext[E] << "\t";
		   }

		 data << endl;
		}



		for(int R=0; R < N; R++) //sets y0 array to be y_n+1
		{
		y0[R] = ynext[R];
		}

		for(int V=0; V < N; V++) //calculates y1 array for step doubling
	        {
                y1[V] = RK(x,y0,deltax)[V];
		}

		count = count + 2;

	}

	cout << "The number of function evaluations is: " << count << endl;	

	return 0;
}
